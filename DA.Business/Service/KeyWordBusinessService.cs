﻿using DA.Business.Interface;
using DA.Repositories.Repositories;
using Entity.Data.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DA.Business.Service
{
    public class KeyWordBusinessService : BaseService<KeyWord>, IKeyWordBusinessService
    {
        private readonly IRepository<KeyWord> repository;
        private readonly IUnitOfWork unitOfWork;
        public KeyWordBusinessService(IRepository<KeyWord> entityRepository, IUnitOfWork unitOfWork) 
            : base(entityRepository, unitOfWork)
        {
            this.repository = entityRepository;
            this.unitOfWork = unitOfWork;
        }

        public async Task<IEnumerable<KeyWord>> GetKeyWordActive()
        {
            return await GetAsync(
                orderBy: item => (item.OrderBy( s => s.KeyWordName)),
                filter: item => item.IsActive == true
                );
        }

        public async Task<IEnumerable<KeyWord>> GetKeyWordByName(string keywordName)
        {
            return await GetAsync(filter: item => item.KeyWordName == keywordName);
        }
    }
}
