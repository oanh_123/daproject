﻿using DA.Business.Interface;
using DA.Repositories.Repositories;
using Entity.Data.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DA.Business.Service
{
    public class EventLogBusinessService : BaseService<EventLog>, IEventLogBusinessService
    {
        private readonly IRepository<EventLog> repository;
        private readonly IUnitOfWork unitOfWork;
        public EventLogBusinessService(IRepository<EventLog> entityRepository, IUnitOfWork unitOfWork) : base(entityRepository, unitOfWork)
        {
            this.repository = entityRepository;
            this.unitOfWork = unitOfWork;
        }

        public async Task<EventLog> GetByAction(int id, string action, DateTime createdDate)
        {
            return await Find(id, action, createdDate);
        }

        public async Task<IEnumerable<EventLog>> GetEventLog()
        {
            return await GetAsync(orderBy: item => item.OrderBy(s => s.DossierNo));
        }
    }
}
