﻿using DA.Business.Interface;
using DA.Repositories.Repositories;
using Entity.Data.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DA.Business.Service
{
    public class SiteBusinessService : BaseService<Site>, ISiteBusinessService
    {
        private readonly IRepository<Site> repository;
        private readonly IUnitOfWork unitOfWork;
        public SiteBusinessService(IRepository<Site> entityRepository, IUnitOfWork unitOfWork) 
            : base(entityRepository, unitOfWork)
        {
            this.repository = entityRepository;
            this.unitOfWork = unitOfWork;
        }

        public async Task<IEnumerable<Site>> Get(int? region = null, int? currentPage = null, int? pageSize = null, bool? isActive = null)
        {
            return await GetAsync(
                filter: item => (region == null || item.RegionId == region) && (isActive ==null || item.IsActive == isActive),
                orderBy: item => (item.OrderBy(s => s.Region.Entity.EntityName).ThenBy(s=> s.Region.RegionName).ThenBy(s => s.SiteName)),
                page: currentPage,
                pageSize: pageSize
                );          
        }

        public async Task<Site> GetSiteById(int id)
        {
            return await GetByIdAsync(id);
        }

        public async Task<IEnumerable<Site>> GetSiteByName(string siteName, int regionId)
        {
            return await GetAsync(filter: item => (item.RegionId == regionId) && (item.SiteName == siteName));
        }
    }
}
