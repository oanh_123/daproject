﻿using DA.Business.Interface;
using DA.Repositories.Repositories;
using Entity.Data.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DA.Business.Service
{
    public class CallObjectBusinessService : BaseService<CallObject>, ICallObjectBusinessService

    {
        private readonly IRepository<CallObject> repository;
        private readonly IUnitOfWork unitOfWork;
        public CallObjectBusinessService(IRepository<CallObject> entityRepository, IUnitOfWork unitOfWork) : base(entityRepository, unitOfWork)
        {
            this.repository = entityRepository;
            this.unitOfWork = unitOfWork;
        }

        public async Task<IEnumerable<CallObject>> Get(int? currentPage = null, int? pageSize = null, bool? isActive = null)
        {
            return await GetAsync(
                filter: item => (isActive == null || item.IsActive == isActive),
                orderBy: item => (item.OrderBy(s => s.CallObjName)),
                page: currentPage,
                pageSize: pageSize
                );
        }

        public async Task<IEnumerable<CallObject>> GetCallObjByName(string callobjName)
        {
            return await GetAsync(filter: item => (item.CallObjName == callobjName));
        }
    }
}
