﻿using DA.Business.Interface;
using DA.Repositories.Repositories;
using Entity.Data.EF;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DA.Business.Service
{
    public class BaseService<TEntity> : IBaseService<TEntity> where TEntity : class
    {
        public IRepository<TEntity> _entityRepository;
        private readonly IUnitOfWork _unitOfWork;
        internal DAContext Context;
        internal DbSet<TEntity> DbSet;
        public BaseService(IRepository<TEntity> entityRepository,
            IUnitOfWork unitOfWork)
        {
            _entityRepository = entityRepository;
            _unitOfWork = unitOfWork;

        }
        public async Task<IEnumerable<TEntity>> GetAllAsync()
        {
            return await _entityRepository.GetAllAsync();

        }

        public async Task<TEntity> GetByIdAsync(int id)
        {
            return await _entityRepository.GetByIdAsync();
        }

        public async Task<int> Insert(TEntity entity)
        {
            try
            {
                _entityRepository.Insert(entity);
                var affectRows = await _unitOfWork.Save();
                return affectRows;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<int> Update(TEntity entity)
        {
            try
            {
                _entityRepository.Update(entity);
                return await _unitOfWork.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<int> Delete(TEntity entity)
        {
            try
            {
                _entityRepository.Delete(entity);
                return await _unitOfWork.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<IEnumerable<TEntity>> GetAsync(Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            string includeProperties = "",
            int? page = null,
            int? pageSize = null)
        {
            return await _entityRepository.GetAsync(filter, orderBy,includeProperties, page, pageSize);
        }

        public async Task<TEntity> Find(params object[] keyValues)
        {
            try
            {
                var resultSearch = _entityRepository.Find(keyValues);
                return resultSearch;

            }
            catch (Exception ex)
            {
                throw ex;

            }
        }

        public async Task<int> Add(TEntity entity)
        {
            try
            {
                _entityRepository.Insert(entity);
                return await _unitOfWork.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}

