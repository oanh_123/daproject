﻿using DA.Business.Interface;
using DA.Repositories.Repositories;
using Entity.Data.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DA.Business.Service
{
    public class UserProfileService : BaseService<UserProfile>, IUserProfileService

    {
        private readonly IRepository<UserProfile> repository;
        private readonly IUnitOfWork unitOfWork;
        public Profile common = new Profile();
        public UserProfileService(IRepository<UserProfile> entityRepository,
            IUnitOfWork unitOfWork) : base(entityRepository, unitOfWork)
        {
            this.repository = entityRepository;
            this.unitOfWork = unitOfWork;
        }

        public async Task<IEnumerable<UserProfile>> Get(int? currentPage = null, int? pageSize = null)
        {
            return await GetAsync(
                orderBy: item => (item.OrderBy(s => s.UserId)),
                page: currentPage,
                pageSize: pageSize);
        }

        public async Task<IEnumerable<UserProfile>> GetUserById(string id)
        {
            return await  GetAsync(filter: item => item.UserId == id && item.IsActive);
        }

        public async Task<IEnumerable<UserProfile>> GetUserByName(string firstName, string lastName)
        {
            return await GetAsync(filter: item => item.FirstName == firstName && item.LastName == lastName);
        }

        public async Task<IEnumerable<UserProfile>> GetByTechAd(string id)
        {
            return await GetAsync(filter: item => item.UserId == id);
        }

        public async Task<IEnumerable<UserProfile>> GetForNormalUser()
        {
            return await GetAsync(filter: item => item.ProfileId !=  (int)DA.Utilities.Common.Profile.TechnicalAdmin);
        }

        public async Task<IEnumerable<UserProfile>> GetJustRelayAlert()
        {
            return await GetAsync(filter: item => item.ProfileId == 1);
            
        }
    }
}
