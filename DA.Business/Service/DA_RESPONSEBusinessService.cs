﻿using DA.Business.Interface;
using DA.Repositories.Repositories;
using Entity.Data.EF;
using System;
using System.Collections.Generic;
using System.Text;

namespace DA.Business.Service
{
    public class DA_RESPONSEBusinessService : BaseService<DaResponse>, IDA_RESPONSEBusinessService

    {
        private readonly IRepository<DaResponse> repository;
        private readonly IUnitOfWork unitOfWork;
        public DA_RESPONSEBusinessService(IRepository<DaResponse> entityRepository, IUnitOfWork unitOfWork) 
            : base(entityRepository, unitOfWork)
        {
            this.repository = entityRepository;
            this.unitOfWork = unitOfWork;
            
        }
    }
}
