﻿using DA.Business.Interface;
using DA.Repositories.Repositories;
using Entity.Data.EF;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;

namespace DA.Business.Service
{     
    public class AttachedFileBusinessService : BaseService<AttachedFiles>, IAttachedFilesBusinessService
    {
        private readonly IRepository<AttachedFiles> repository;
        private readonly IUnitOfWork unitOfWork;
        public DAContext _context = new DAContext();
        public AttachedFileBusinessService(IRepository<AttachedFiles> entityRepository, IUnitOfWork unitOfWork) : base(entityRepository, unitOfWork)
        {
            this.repository = entityRepository;
            this.unitOfWork = unitOfWork;
        }

        public async Task<IEnumerable<AttachedFiles>> GetAttachedFileById(int pID)
        {
            return await GetAsync(filter: item => item.AttachedFileId == pID);
        }

        public  async Task<IEnumerable<AttachedFiles>> GetByFileID(int pFileID)
        {
            return await GetAsync(filter: item => item.FileNo == pFileID);
        }

        public async Task<bool> isExistFileName(string pFileName)
        {
            bool isExist = _context.AttachedFiles.Any(m => m.FileName == pFileName);
            return   isExist;
        }
    }
}
