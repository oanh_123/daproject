﻿using DA.Business.Interface;
using DA.Repositories.Repositories;
using Entity.Data.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
using DA.Utilities.Common;

namespace DA.Business.Service
{
    public class DABusinessService : BaseService<Da>, IDABusinessService
    {
        private IRepository<Da> _dARepository;
        private IRepository<RelayLog> _logRepository;
        private IRepository<KeyWord> _keyRepository;
        private readonly IUnitOfWork unitOfWork;
        private IRepository<UsersLog> entityRepository;
        private DAContext _context = new DAContext();
        private const string ZERO_LIST = "00000000";
        public DABusinessService(IRepository<Da> entityRepository, IUnitOfWork unitOfWork)
            : base(entityRepository, unitOfWork)
        {
            this._dARepository = new Repository<Da>(_context);
            this._logRepository = new Repository<RelayLog>(_context);
            this._keyRepository = new Repository<KeyWord>(_context);
            this.unitOfWork = new UnitOfWork(_context);
        }

        public async Task<IQueryable<DACustom>> BuildSelectQuery()
        {
            var getNullSiteIdRecords = from da in _context.Da
                                        where da.SiteId == null
                                        select da;
            var nullSiteIdRecords = (from da in getNullSiteIdRecords
                                     join co in _context.CallObject on da.CallObjId equals co.CallObjId
                                     select new DACustom
                                     {
                                         FileNo = da.FileNo,
                                         FileNoString = (ZERO_LIST + da.FileNo.ToString()).Substring((ZERO_LIST + da.FileNo.ToString()).Length - 8, 8),
                                         CallingDate = da.CallingDate,
                                         CallObjID = co.CallObjId,
                                         CallObjName = co.CallObjName,
                                         SiteID = da.SiteId,
                                         SiteName = string.Empty,
                                         EntityID = null,
                                         EntityName = string.Empty,
                                         RegionID = null,
                                         Status = da.Status,
                                         StatusDescription = da.StatusNavigation.Description,
                                         StatusFrenchDescription = da.StatusNavigation.DescriptionFrench
                                     });
            IQueryable<DACustom> vQuery =  (from da in _context.Da
                                                              join co in _context.CallObject on da.CallObjId equals co.CallObjId
                                                              join s in _context.Site on da.SiteId equals s.SiteId
                                                              join r in _context.Region on s.RegionId equals r.RegionId
                                                              join e in _context.Entity on r.EntityId equals e.EntityId
                                                              select new DACustom
                                                              {
                                                                  FileNo = da.FileNo,
                                                                  FileNoString = (ZERO_LIST + da.FileNo.ToString()).Substring((ZERO_LIST + da.FileNo.ToString()).Length - 8, 8),
                                                                  CallingDate = da.CallingDate,
                                                                  CallObjID = co.CallObjId,
                                                                  CallObjName = co.CallObjName,
                                                                  SiteID = s.SiteId,
                                                                  SiteName = s.SiteName,
                                                                  EntityID = e.EntityId,
                                                                  EntityName = e.EntityName + " - " + r.RegionName,
                                                                  RegionID = s.RegionId,
                                                                  Status = da.Status,
                                                                  StatusDescription = da.StatusNavigation.Description,
                                                                  StatusFrenchDescription = da.StatusNavigation.DescriptionFrench
                                                              }
                  ).Union(nullSiteIdRecords).OrderBy(u => u.FileNoString);//

            return vQuery;
        }

        public async Task<IQueryable<DACustom>> BuildSelectQueryForPrestataire()
        {
            IQueryable<DACustom> vQuery = (from da in _context.Da
                                           join co in _context.CallObject on da.CallObjId equals co.CallObjId
                                           select new DACustom
                                           {
                                               FileNo = da.FileNo,
                                               FileNoString = (ZERO_LIST + da.FileNo.ToString()).Substring((ZERO_LIST + da.FileNo.ToString()).Length - 8, 8),
                                               CallingDate = da.CallingDate,
                                               CallObjID = co.CallObjId,
                                               CallObjName = co.CallObjName,
                                               SiteID = da.SiteId,
                                               Status = da.Status,
                                               StatusDescription = da.StatusNavigation.Description,
                                               StatusFrenchDescription = da.StatusNavigation.DescriptionFrench
                                           }).OrderBy(s => s.FileNoString);
            return vQuery;
        }

        public async Task<IQueryable<DACustom>> BuildSelectQueryWithRegionList(IEnumerable<int> pRegionList)
        {
            IQueryable<DACustom> vQuery = (from da in _context.Da
                                                              join co in _context.CallObject on da.CallObjId equals co.CallObjId
                                                              join s in _context.Site on da.SiteId equals s.SiteId
                                                              join r in _context.Region on s.RegionId equals r.RegionId
                                                              join e in _context.Entity on r.EntityId equals e.EntityId
                                                              where pRegionList.Any(a => s.RegionId == a)
                                                              select new DACustom
                                                              {
                                                                  FileNo = da.FileNo,
                                                                  FileNoString = (ZERO_LIST + da.FileNo.ToString()).Substring((ZERO_LIST + da.FileNo.ToString()).Length - 8, 8),
                                                                  CallingDate = da.CallingDate,
                                                                  CallObjID = co.CallObjId,
                                                                  CallObjName = co.CallObjName,
                                                                  SiteID = s.SiteId,
                                                                  SiteName = s.SiteName,
                                                                  EntityID = e.EntityId,
                                                                  EntityName = e.EntityName + " - " + r.RegionName,
                                                                  RegionID = s.RegionId,
                                                                  Status = da.Status,
                                                                  StatusDescription = da.StatusNavigation.Description,
                                                                  StatusFrenchDescription = da.StatusNavigation.DescriptionFrench
                                                              }
                   ).OrderBy(u => u.FileNoString);

            return vQuery;
        }

        public Task<int> Delete(string id)
        {
            try
            {
                _dARepository.Delete(id);
                return unitOfWork.Save();
                
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public async Task<IQueryable<DACustom>> FilterSearchResult(IQueryable<DACustom> pFilterResult, int? pFileNo, int? pCallObjID, int? pSiteID, DateTime? pCallingDateFrom, DateTime? pCallingDateTo, int? pRegionID, int? pUserRole)
        {
            if (pFileNo.HasValue && pFileNo.Value > 0)
                pFilterResult = pFilterResult.Where(s => s.FileNo.ToString().Contains(pFileNo.ToString()));

            if (pCallObjID.HasValue && pCallObjID.Value > 0)
                pFilterResult = pFilterResult.Where(s => s.CallObjID == pCallObjID);
           
            if (pSiteID.HasValue && pSiteID.Value > 0)
                pFilterResult = pFilterResult.Where(s => s.SiteID == pSiteID);

            if (pCallingDateFrom != null)
                pFilterResult = pFilterResult.Where(s => s.CallingDate >= pCallingDateFrom);

            if (pCallingDateTo != null)
                pFilterResult = pFilterResult.Where(s => s.CallingDate <= pCallingDateTo);

            if (pRegionID.HasValue && pRegionID.Value > 0)
                pFilterResult = pFilterResult.Where(s => s.RegionID == pRegionID);

            if (pUserRole.HasValue && pUserRole.Value > 0)
            {
                switch ((DA.Utilities.Common.Profile)pUserRole)
                {
                    case DA.Utilities.Common.Profile.RelayAlert:
                        pFilterResult = pFilterResult.Where(s => s.Status != (int)DA.Utilities.Common.Status.Created);
                        break;
                    case DA.Utilities.Common.Profile.External:
                        pFilterResult = pFilterResult.Where(s => s.Status == (int)DA.Utilities.Common.Status.Created || s.Status == (int)DA.Utilities.Common.Status.Responded || s.Status == (int)DA.Utilities.Common.Status.Closed);
                        break;
                    case DA.Utilities.Common.Profile.FunctionalAdmin:
                        pFilterResult = pFilterResult.Where(s => s.Status != (int)DA.Utilities.Common.Status.Created);
                        break;

                }
            }
            return pFilterResult;
        }


        public async  Task<Da> GetById(int id)
        {
            var listDA = await GetAsync(filter: item => item.FileNo == id,
                includeProperties: "DA_CLOSE,DA_RELAY,DA_RESPONSE");
            return listDA.FirstOrDefault();
        }

        public async Task<IEnumerable<DACustom>> GetDASearchResult(IEnumerable<int> pRegionList, int? pFileNo, int? pCallObjID, int? pSiteID, DateTime? pCallingDateFrom, DateTime? pCallingDateTo, int? pRegionID, int pCurrentPage, int pPageSize, int? pUserRole)
        {
            IQueryable<DACustom> vQuery = null;
            if (pRegionList == null)
                vQuery = await BuildSelectQuery();
            else
                vQuery = await BuildSelectQueryWithRegionList(pRegionList);

            vQuery =  await FilterSearchResult(vQuery, pFileNo, pCallObjID, pSiteID, pCallingDateFrom, pCallingDateTo, pRegionID, pUserRole);
            vQuery = vQuery.OrderByDescending(x => x.FileNo).Skip((pCurrentPage - 1) * pPageSize).Take(pPageSize);
            return vQuery.ToList();
        }

        public async Task<int> GetDASearchResultCount(IEnumerable<int> pRegionList, int? pFileNo, int? pCallObjID, int? pSiteID, DateTime? pCallingDateFrom, DateTime? pCallingDateTo, int? pRegionID, int? pUserRole)
        {
            IQueryable<DACustom> vQuery = null;
            if (pRegionList == null)
                vQuery = await BuildSelectQuery();
            else
                vQuery = await BuildSelectQueryWithRegionList(pRegionList);

            vQuery = await FilterSearchResult(vQuery, pFileNo, pCallObjID, pSiteID, pCallingDateFrom, pCallingDateTo, pRegionID, pUserRole);
            return  vQuery.OrderByDescending(x => x.FileNo).Count();

        }

        public List<int> GetInvolveFile(string SuffererGGID, int userRole)
        {
            var anonymousUserFN = ConfigHelper.AnonymousPersonFirstName;
            var anonymousUserLN = ConfigHelper.AnonymousPersonLastName;
            if((DA.Utilities.Common.Profile)userRole == DA.Utilities.Common.Profile.RelayAlert)
                return _context.Da.Where(s => s.SuffererGgid == SuffererGGID && s.Status != (int)DA.Utilities.Common.Status.Created
                                            && string.Compare(s.SuffererFirstName, anonymousUserFN, true) != 0
                                            && string.Compare(s.SuffererLastName, anonymousUserLN, true) != 0).Select(m => m.FileNo).ToList();
            else if ((DA.Utilities.Common.Profile)userRole == DA.Utilities.Common.Profile.External)
                return _context.Da.Where(s => s.SuffererGgid == SuffererGGID && (s.Status == (int)DA.Utilities.Common.Status.Created ||
                                                                               s.Status == (int)DA.Utilities.Common.Status.Responded ||
                                                                               s.Status == (int)DA.Utilities.Common.Status.Closed)
                                                                            && string.Compare(s.SuffererFirstName, anonymousUserFN, true) != 0
                                                                            && string.Compare(s.SuffererLastName, anonymousUserLN, true) != 0).Select(m => m.FileNo).ToList();
            else
                return _context.Da.Where(s => s.SuffererGgid == SuffererGGID && s.Status != (int)DA.Utilities.Common.Status.Created
                                            && string.Compare(s.SuffererFirstName, anonymousUserFN, true) != 0
                                            && string.Compare(s.SuffererLastName, anonymousUserLN, true) != 0).Select(m => m.FileNo).ToList();

        }

        public int GetLastestFileNo()
        {
            var list = _dARepository.GetAllAsync();
            if (list.Result.Count() > 0)
                return list.Result.Max(s => s.FileNo);
            else
                return 0;
        }

        public async Task<int> Insert(Da dA, string comment = null)
        {
            try
            {
                if (!string.IsNullOrEmpty(comment))
                {
                    var lstKeyword = await _keyRepository.GetAsync(filter: x => x.IsActive == true);
                    foreach (var keyword in lstKeyword)
                    {
                        if (comment.Contains(keyword.KeyWordName))
                        {
                            dA.DaResponse.KeywordResponse.Add(keyword);
                        }
                    }

                }

                _dARepository.Insert(dA);
                await unitOfWork.Save();
               await new UserLogBusinessService(entityRepository,unitOfWork).Insert(dA.DaResponse.UpdatedBy, (int)ActionType.Create, "DA", dA.FileNo, null);
                return dA.FileNo;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public async Task<int> Update(Da dA, string comment = null)
        {
            try
            {
                var lstKeyword = await _keyRepository.GetAsync(filter: item => item.IsActive == true);
                if (!string.IsNullOrEmpty(comment))
                {
                    if (dA.Status == (int)DA.Utilities.Common.Status.Created || dA.Status == (int)DA.Utilities.Common.Status.Responded)
                    {
                        dA.DaResponse.KeywordResponse.Clear();
                        foreach (var keyword in lstKeyword)
                        {
                            if (comment.Contains(keyword.KeyWordName))
                            {
                                dA.DaResponse.KeywordResponse.Add(keyword);
                            }
                        }
                    }
                    else if (dA.Status == (int)DA.Utilities.Common.Status.Relayed || dA.Status == (int)DA.Utilities.Common.Status.SaveClosedComment || dA.Status == (int)DA.Utilities.Common.Status.ReadyForClosed)
                    {
                        dA.DaRelay.KeywordRelay.Clear();
                        foreach (var keyword in lstKeyword)
                        {
                            if (comment.Contains(keyword.KeyWordName))
                            {
                                //dA.DaRelay.KeywordRelay.Add(keyword);
                            }
                        }
                    }
                }
                else
                {
                    if (dA.Status == (int)DA.Utilities.Common.Status.Created || dA.Status == (int)DA.Utilities.Common.Status.Responded)
                    {
                        dA.DaResponse.KeywordResponse.Clear();
                    }
                    else if (dA.Status == (int)DA.Utilities.Common.Status.Relayed || dA.Status == (int)DA.Utilities.Common.Status.SaveClosedComment || dA.Status == (int)DA.Utilities.Common.Status.ReadyForClosed)
                    {
                        dA.DaRelay.KeywordRelay.Clear();
                    }
                }

                _dARepository.Update(dA);
               await  unitOfWork.Save();

                if (dA.Status == (int)DA.Utilities.Common.Status.Created || dA.Status == (int)DA.Utilities.Common.Status.Responded)
                   await new UserLogBusinessService(entityRepository, unitOfWork).Insert(dA.DaResponse.UpdatedBy, (int)ActionType.Update, "DA(Extern Update)", dA.FileNo, null);
                else if (dA.Status == (int)DA.Utilities.Common.Status.Relayed || dA.Status == (int)DA.Utilities.Common.Status.SaveClosedComment || dA.Status == (int)DA.Utilities.Common.Status.ReadyForClosed)
                    await new UserLogBusinessService(entityRepository, unitOfWork).Insert(dA.DaRelay.UpdatedBy, (int)ActionType.Update, "DA(Relay update)", dA.FileNo, null);
                else if (dA.Status == (int)DA.Utilities.Common.Status.Closed)
                   await new UserLogBusinessService(entityRepository, unitOfWork).Insert(dA.DaClose.UpdatedBy, (int)ActionType.Update, "DA(Closed update)", dA.FileNo, null);
                return dA.FileNo;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<int> UpdateAndLog(Da dA, RelayLog relayLog, string relayComment = null)
        {
            try
            {
                _dARepository.Update(dA);
                if(dA.Status == (int)DA.Utilities.Common.Status.Relayed)
                {
                    _logRepository.Insert(relayLog);
                }
                await unitOfWork.Save();
                return dA.FileNo;
            }
            catch(Exception ex)
            {
                unitOfWork.RollBack();
                throw ex;
            }
        }

    }
}
