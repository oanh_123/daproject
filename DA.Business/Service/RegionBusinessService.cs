﻿using DA.Business.Interface;
using DA.Repositories.Repositories;
using Entity.Data.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DA.Business.Service
{
    public class RegionBusinessService : BaseService<Region>, IRegionBusinessService
    {
        private readonly IRepository<Region> repository;
        private readonly IUnitOfWork unitOfWork;
        public RegionBusinessService(IRepository<Region> entityRepository, IUnitOfWork unitOfWork) 
            : base(entityRepository, unitOfWork)
        {
            this.repository = entityRepository;
            this.unitOfWork = unitOfWork;
        }

        public async Task<IEnumerable<Region>> GetRegion(int? region = null, int? currentPage = null, int? pageSize = null, bool? isActive = null)
        {
            return await GetAsync(
                filter: item => (region == null || item.EntityId == region) && (isActive == null || item.IsActive == isActive),
                orderBy: item => item.OrderBy (s=>s.Entity.EntityName).ThenBy(s=>s.RegionName),
                page: currentPage,
                pageSize: pageSize
                );
        }

        public async Task<Region> GetRegionById(int id)
        {
            return await GetByIdAsync(id);
            
        }

        public async Task<IEnumerable<Region>> GetRegionByIdList(List<int> RegionIdList)
        {
            return await GetAsync( filter: item => item.RegionId.Equals(RegionIdList));
        }

        public async Task<IEnumerable<Region>> GetRegionByName(string regionName, int entityId)
        {
            return await GetAsync(
                filter: item => (item.EntityId == entityId) && (item.RegionName == regionName)
                );
        }
    }
}
