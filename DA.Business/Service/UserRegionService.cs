﻿using DA.Business.Interface;
using DA.Repositories.Repositories;
using Entity.Data.EF;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;

namespace DA.Business.Service
{
    public class UserRegionService : BaseService<UserRegion>, IUserRegion
    {
        private readonly IRepository<UserRegion> repository;
        private readonly IUnitOfWork unitOfWork;
        private DAContext _context = new DAContext();
        public UserRegionService(IRepository<UserRegion> entityRepository,
            IUnitOfWork unitOfWork) : base(entityRepository, unitOfWork)
        {
            this.repository = entityRepository;
            this.unitOfWork = unitOfWork;
        }

        public async Task<UserRegion> GetBy(string userID, int regionID)
        {
            return await Find(userID, regionID);
        }

        public async Task<IEnumerable<UserRegion>> GetByRegion(int regionID)
        {
            return await GetAsync(filter: item => item.RegionId == regionID,
                includeProperties: "UserProfile,Region");
        }

        public IEnumerable<int> GetListRegionByUser(string userID)
        {
            return _context.UserRegion.ToList()
                .Join(_context.Region.ToList(), ur => ur.RegionId, r => r.RegionId, (ur, r) => new { ur, r })
                .Where(joined => joined.ur.UserId == userID && joined.ur.IsActive && joined.r.IsActive)
                .Select(result => result.r.RegionId);             
        }
    }
}
