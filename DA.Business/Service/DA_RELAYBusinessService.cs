﻿using DA.Business.Interface;
using DA.Repositories.Repositories;
using Entity.Data.EF;
using System;
using System.Collections.Generic;
using System.Text;

namespace DA.Business.Service
{
    public class DA_RELAYBusinessService : BaseService<DaRelay>, IDA_RELAYBusinessService
    {
        private readonly IRepository<DaRelay> repository;
        private readonly IUnitOfWork unitOfWork;
        public DA_RELAYBusinessService(IRepository<DaRelay> entityRepository, IUnitOfWork unitOfWork)
            : base(entityRepository, unitOfWork)
        {
            this.repository = entityRepository;
            this.unitOfWork = unitOfWork;
        }
    }
}
