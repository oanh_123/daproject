﻿using DA.Business.Interface;
using DA.Repositories.Repositories;
using Entity.Data.EF;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;

namespace DA.Business.Service
{
    public class FileNoManagementBusinessService : BaseService<FileNoManagement>,IFileNoManagementBusinessService
    {
        private readonly IRepository<FileNoManagement> repository;
        private readonly IUnitOfWork unitOfWork;
        private DAContext _context = new DAContext();
        public FileNoManagementBusinessService(IRepository<FileNoManagement> entityRepository, IUnitOfWork unitOfWork) 
            : base(entityRepository, unitOfWork)
        {
            this.repository = entityRepository;
            this.unitOfWork = unitOfWork;
        }

        public async Task<FileNoManagement> GetCurrent(int currentFileNo)
        {
            var item1 = GetAllAsync().Result.FirstOrDefault();
            var item2 = new FileNoManagement();

            await Delete(item1);
            item2.CurrentFileNo = currentFileNo + 1;
            await Insert(item2);
            var result = await GetAllAsync();
            return result.FirstOrDefault();
           
        }
    }
}
