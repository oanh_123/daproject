﻿using DA.Business.Interface;
using DA.Repositories.Repositories;
using DA.Utilities.Common;
using Entity.Data.EF;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DA.Business.Service
{
    public class UserLogBusinessService : BaseService<UsersLog>, IUserLogBusinessService
    {
        private readonly IRepository<UsersLog> repository;
        private readonly IUnitOfWork unitOfWork;
        public UserLogBusinessService(IRepository<UsersLog> entityRepository, IUnitOfWork unitOfWork) : base(entityRepository, unitOfWork)
        {
            this.repository = entityRepository;
            this.unitOfWork = unitOfWork;
        }

        public  async Task<UsersLog> CommonObject(string loginId, int actionType, string tableName, int tableKeyInt, string fileName, string tableKeyString)
        {
            var _object = new UsersLog();
            ActionType actionTypeString = (ActionType)actionType;
            _object.LoginId = loginId;
            _object.EventDate = DateTime.Now;
            _object.Action = actionTypeString.ToString();
            if(actionType != (int)ActionType.Connect)
            {
                if (actionType == (int)ActionType.Create || actionType == (int)ActionType.Update)
                    _object.Detail = string.Format("Table: {0}, Record ID : {1}", tableName, string.IsNullOrEmpty(tableKeyString) ? tableKeyInt.ToString() : tableKeyString);
                else
                {
                    if (actionType == (int)ActionType.Download)
                        _object.Detail = string.Format("Download file : {0}", fileName);
                    if (actionType == (int)ActionType.UpLoadDocument)
                        _object.Detail = string.Format("Upload file : {0}", fileName);
                }
            }
            return _object;
        }

        public Task<int> Insert(string loginId, int actionType, string tableName, int tableKey, string fileName)
        {
            try
            {
                var _object = CommonObject(loginId, actionType, tableName, tableKey, fileName, null);
                //repository.Insert(_object);
                return unitOfWork.Save();
            }
            catch(Exception ex)
            {
                //return 0;
                throw ex;
            }
        }
    }
}
