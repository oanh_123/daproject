﻿using DA.Business.Interface;
using DA.Repositories.Repositories;
using Entity.Data.EF;
using System;
using System.Collections.Generic;
using System.Text;

namespace DA.Business.Service
{
    public class DA_CLOSEBusinessService : BaseService<DaClose>, IDA_CLOSEBusinessService

    {
        private readonly IRepository<DaClose> repository;
        private readonly IUnitOfWork unitOfWork;
        public DA_CLOSEBusinessService(IRepository<DaClose> entityRepository, IUnitOfWork unitOfWork) :
            base(entityRepository, unitOfWork)
        {
            this.repository = entityRepository;
            this.unitOfWork = unitOfWork;
        }
    }
}
