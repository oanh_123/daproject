﻿using DA.Business.Interface;
using DA.Repositories.Repositories;
using Entity.Data.EF;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Linq;
namespace DA.Business.Service
{
    public class RoleMatrixBusinessService : BaseService<RoleMatrix>, IRoleMatrixBusinessService
    {
        private readonly IRepository<RoleMatrix> repository;
        private readonly IUnitOfWork unitOfWork;
        public DAContext _context = new DAContext();
        public RoleMatrixBusinessService(IRepository<RoleMatrix> entityRepository, IUnitOfWork unitOfWork) : 
            base(entityRepository, unitOfWork)
        {
            this.repository = entityRepository;
            this.unitOfWork = unitOfWork;
        }

        public async Task<IEnumerable<bool>> GetListRoleByProfileID(int profileID)
        {
            switch(profileID)
            {
                case (int)DA.Utilities.Common.Profile.RelayAlert:
                    return (from rm in _context.RoleMatrix
                            select rm.RelayAlert
                            ).ToList();
                case (int)DA.Utilities.Common.Profile.External:
                    return (from rm in _context.RoleMatrix
                            select rm.External
                            ).ToList();
                case (int)DA.Utilities.Common.Profile.FunctionalAdmin:
                    return (from rm in _context.RoleMatrix
                            select rm.FunctionalAdmin).ToList();
                default:
                    return (from rm in _context.RoleMatrix
                            select rm.External
                            ).ToList();
            }
        }
    }
}
