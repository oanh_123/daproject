﻿using DA.Business.Interface;
using DA.Repositories.Repositories;
using Entity.Data.EF;
using System;
using System.Collections.Generic;
using System.Text;

namespace DA.Business.Service
{
    public class AgeRangeBusinessService : BaseService<AgeRange>, IAgeRangeBusinessService

    {
        private readonly IRepository<AgeRange> repository;
        private readonly IUnitOfWork unitOfWork;
        public AgeRangeBusinessService(IRepository<AgeRange> entityRepository, IUnitOfWork unitOfWork) : base(entityRepository, unitOfWork)
        {
            this.repository = entityRepository;
            this.unitOfWork = unitOfWork;
        }
    }
}
