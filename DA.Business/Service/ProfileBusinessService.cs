﻿using DA.Business.Interface;
using DA.Repositories.Repositories;
using Entity.Data.EF;
using System;
using System.Collections.Generic;
using System.Text;

namespace DA.Business.Service
{
    public class ProfileBusinessService : BaseService<Profile>, IProfileBusinessService
    {
        private readonly IRepository<Profile> repository;
        private readonly IUnitOfWork unitOfWork;
        public ProfileBusinessService(IRepository<Profile> entityRepository, IUnitOfWork unitOfWork) 
            : base(entityRepository, unitOfWork)
        {
            this.repository = entityRepository;
            this.unitOfWork = unitOfWork;
        }
    }
}
