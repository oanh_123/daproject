﻿using Entity.Data.EF;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DA.Business.Interface
{
    public interface IUserLogBusinessService : IBaseService<UsersLog>
    {
        Task<UsersLog> CommonObject(string loginId, int actionType, string tableName, int tableKeyInt, string fileName, string tableKeyString);
        Task<int> Insert(string loginId, int actionType, string tableName, int tableKey, string fileName);
    }
}
