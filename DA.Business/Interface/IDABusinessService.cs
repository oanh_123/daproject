﻿using Entity.Data.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DA.Business.Interface
{
    public interface IDABusinessService : IBaseService<Da>
    {
        int GetLastestFileNo();
        List<int> GetInvolveFile(string SuffererGGID, int userRole);
        Task<int> GetDASearchResultCount(IEnumerable<int> pRegionList, int? pFileNo, int? pCallObjID, int? pSiteID,
            DateTime? pCallingDateFrom, DateTime? pCallingDateTo, int? pRegionID, int? pUserRole);
        Task<IEnumerable<Entity.Data.EF.DACustom>> GetDASearchResult(IEnumerable<int> pRegionList, int? pFileNo,
            int? pCallObjID, int? pSiteID, DateTime? pCallingDateFrom, DateTime? pCallingDateTo, int? pRegionID, int pCurrentPage, int pPageSize, int? pUserRole);
        Task<IQueryable<Entity.Data.EF.DACustom>> FilterSearchResult(IQueryable<Entity.Data.EF.DACustom> pFilterResult, int? pFileNo, int? pCallObjID, int? pSiteID,
            DateTime? pCallingDateFrom, DateTime? pCallingDateTo, int? pRegionID, int? pUserRole);
        Task<Da> GetById(int id);
        Task<int> Insert(Da dA, string comment = null);
        Task<int> Update(Da dA, string comment = null);
        Task<int> UpdateAndLog(Da dA, RelayLog relayLog, string relayComment = null);
        Task<int> Delete(string id);
        Task<IQueryable<DACustom>> BuildSelectQuery();
        Task<IQueryable<DACustom>> BuildSelectQueryForPrestataire();
        Task<IQueryable<DACustom>> BuildSelectQueryWithRegionList(IEnumerable<int> pRegionList);

    }
}
