﻿using Entity.Data.EF;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DA.Business.Interface
{
    public interface IFileNoManagementBusinessService : IBaseService<FileNoManagement>
    {
        Task<FileNoManagement> GetCurrent(int currentFileNo);
        
    }
}
