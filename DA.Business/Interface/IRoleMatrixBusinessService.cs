﻿using Entity.Data.EF;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DA.Business.Interface
{
    public interface IRoleMatrixBusinessService : IBaseService<RoleMatrix>
    {
        Task<IEnumerable<bool>> GetListRoleByProfileID(int profileID);
    }
}
