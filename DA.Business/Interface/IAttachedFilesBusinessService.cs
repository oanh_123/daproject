﻿using Entity.Data.EF;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DA.Business.Interface
{
    public interface IAttachedFilesBusinessService : IBaseService<AttachedFiles>
    {
        Task<IEnumerable<AttachedFiles>> GetAttachedFileById(int pID);
        Task<IEnumerable<AttachedFiles>> GetByFileID(int pFileID);
        Task<bool> isExistFileName(string pFileName);
    }
}
