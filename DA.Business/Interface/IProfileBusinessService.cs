﻿using Entity.Data.EF;
using System;
using System.Collections.Generic;
using System.Text;

namespace DA.Business.Interface
{
    public interface IProfileBusinessService : IBaseService<Profile>
    {
    }
}
