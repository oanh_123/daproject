﻿using Entity.Data.EF;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DA.Business.Interface
{
     public interface ICallObjectBusinessService : IBaseService<CallObject>
    {
        Task<IEnumerable<CallObject>> Get(int? currentPage = null, int? pageSize = null, bool? isActive = null);
        Task<IEnumerable<CallObject>> GetCallObjByName(string callobjName);

    }
}
