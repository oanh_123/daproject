﻿using Entity.Data.EF;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DA.Business.Interface
{
   public interface ISiteBusinessService : IBaseService<Site>
    {
        Task<IEnumerable<Site>> Get(int? region = null, int? currentPage = null, int? pageSize = null,
            bool? isActive = null);
        Task<IEnumerable<Site>> GetSiteByName(string siteName, int regionId);
        Task<Site> GetSiteById(int id);
    }
}
