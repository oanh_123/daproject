﻿using Entity.Data.EF;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DA.Business.Interface
{
    public interface IKeyWordBusinessService : IBaseService<KeyWord>
    {
        Task<IEnumerable<KeyWord>> GetKeyWordActive();
        Task<IEnumerable<KeyWord>> GetKeyWordByName(string keywordName);

    }
}
