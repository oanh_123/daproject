﻿using Entity.Data.EF;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DA.Business.Interface
{
    public interface IEventLogBusinessService : IBaseService<EventLog>
    {
        Task<IEnumerable<EventLog>> GetEventLog();
        Task<EventLog> GetByAction(int id, string action, DateTime createdDate);
    }
}
