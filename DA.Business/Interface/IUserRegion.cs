﻿
using Entity.Data.EF;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DA.Business.Interface
{
    public interface IUserRegion : IBaseService<UserRegion>
    {
        IEnumerable<int> GetListRegionByUser(string userID);
        Task<IEnumerable<UserRegion>> GetByRegion(int regionID);
        Task<UserRegion> GetBy(string userID, int regionID);

    }
}
