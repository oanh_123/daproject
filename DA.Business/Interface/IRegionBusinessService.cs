﻿using Entity.Data.EF;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DA.Business.Interface
{
    public interface IRegionBusinessService : IBaseService<Region>
    {
        Task<IEnumerable<Region>> GetRegion(int? region = null, int? currentPage = null, int? pageSize = null,
           bool? isActive = null);
        Task<IEnumerable<Region>> GetRegionByName(string regionName, int entityId);
        Task<IEnumerable<Region>> GetRegionByIdList(List<int> RegionIdList);
        Task<Region> GetRegionById(int id);

    }
}
