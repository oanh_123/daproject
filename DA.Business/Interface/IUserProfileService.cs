﻿using Entity.Data.EF;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DA.Business.Interface
{
   public interface IUserProfileService : IBaseService<UserProfile>
    {
        Task<IEnumerable<UserProfile>> GetForNormalUser();
        Task<IEnumerable<UserProfile>> GetJustRelayAlert();
        Task<IEnumerable<UserProfile>> Get(int? currentPage = null, int? pageSize = null);
        Task<IEnumerable<UserProfile>> GetUserById(string id);
        Task<IEnumerable<UserProfile>> GetUserByName(string firstName, string lastName);
        Task<IEnumerable<UserProfile>> GetByTechAd(string id);

    }
}
