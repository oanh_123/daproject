﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DA.Utilities.Common
{
   public enum Profile
    {
        RelayAlert = 1,
        External = 2,
        FunctionalAdmin = 3,
        TechnicalAdmin = 4
    }
    public enum ParameterTable
    {
        Site = 1,
        Region = 2,
        CallObject = 3,
        KeyWord = 4,
        UserProfile = 5
    }

    public enum ReportType
    {
        Excel,
        PDF
    }

    public enum EmailType
    {
        New_DA,
        Close_DA
    }

    public class ScreenName
    {
        public const string ParameterSite = "1";
        public const string ParameterRegion = "2";
        public const string ParameterProfile = "3";
        public const string ParameterCallObject = "4";
        public const string ParameterKeyWord = "5";
        public const string DANew = "6";
        public const string DAEdit = "7";
        public const string DARelayAlertComment = "8";
        public const string DACloseComment = "9";
        public const string DACloseStatus = "10";
        public const string SearchFull = "11";
        public const string SearchBased = "12";
        public const string ReportingConso = "13";
        public const string ReportingRA = "14";
    }

    public enum Status
    {
        Created = 1,
        Responded = 2,
        Relayed = 3,
        SaveClosedComment = 4,
        ReadyForClosed = 5,
        Closed = 6,

    }

    public enum ChangeTextType
    {
        Close = 1,
        Response = 2,
        Relay = 3
    }

    //EventLog
    public enum ActionType
    {
        Connect = 1,
        Create = 2,
        Update = 3,
        UpLoadDocument = 4,
        Download = 5
    }
}
