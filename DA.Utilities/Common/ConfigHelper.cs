﻿using System;
using System.Configuration;
namespace DA.Utilities.Common
{
    public class ConfigHelper
    {
        #region LDAP
        private static string mLDAPConnectionString = null;
        public static string LDAPConnectionString
        {
            get
            {
                if (mLDAPConnectionString == null)
                    mLDAPConnectionString =  ConfigurationManager.AppSettings["LDAPConnectionString"];

                return mLDAPConnectionString;
            }
        }
        #endregion
        #region Label
        private static string mDefaultLabelDropdown = null;
        public static string DefaultLabelDropdown
        {
            get
            {
                if (mDefaultLabelDropdown == null)
                    mDefaultLabelDropdown = WebConfigurationManager.AppSettings["DefaultLabelDropdown"];

                return mDefaultLabelDropdown;
            }
        }
        #endregion

        #region Email
        private static string mMailSender = null;
        public static string MailSender
        {
            get
            {
                if (mMailSender == null)
                    mMailSender = WebConfigurationManager.AppSettings["MailSender"];

                return mMailSender;
            }
        }
        private static string mEmailSubject_NewDA = null;
        public static string EmailSubject_NewDA
        {
            get
            {
                if (mEmailSubject_NewDA == null)
                    mEmailSubject_NewDA = WebConfigurationManager.AppSettings["EmailSubject_NewDA"];

                return mEmailSubject_NewDA;
            }
        }
        private static string mEmailSubject_CloseDA = null;
        public static string EmailSubject_CloseDA
        {
            get
            {
                if (mEmailSubject_CloseDA == null)
                    mEmailSubject_CloseDA = WebConfigurationManager.AppSettings["EmailSubject_CloseDA"];

                return mEmailSubject_CloseDA;
            }
        }
        private static string mEmailContent_NewDA = null;
        public static string EmailContent_NewDA
        {
            get
            {
                if (mEmailContent_NewDA == null)
                    mEmailContent_NewDA = WebConfigurationManager.AppSettings["EmailContent_NewDA"];

                return mEmailContent_NewDA;
            }
        }
        private static string mEmailContent_CloseDA = null;
        public static string EmailContent_CloseDA
        {
            get
            {
                if (mEmailContent_CloseDA == null)
                    mEmailContent_CloseDA = WebConfigurationManager.AppSettings["EmailContent_CloseDA"];

                return mEmailContent_CloseDA;
            }
        }
        private static string mSendAllMailsTo = null;
        public static string SendAllMailsTo
        {
            get
            {
                if (mSendAllMailsTo == null)
                    mSendAllMailsTo = WebConfigurationManager.AppSettings["SendAllMailsTo"];

                return mSendAllMailsTo;
            }
        }

        //Thien NGUYEN - DA Evolution LOT2 - Resend Email to RA
        private static string mEmailSubject_ResentNewDA = null;
        public static string EmailSubject_ResentNewDA
        {
            get
            {
                if (mEmailSubject_ResentNewDA == null)
                    mEmailSubject_ResentNewDA = WebConfigurationManager.AppSettings["EmailSubject_ResentNewDA"];

                return mEmailSubject_ResentNewDA;
            }
        }

        private static string mEmailContent_ResentNewDA = null;
        public static string EmailContent_ResentNewDA
        {
            get
            {
                if (mEmailContent_ResentNewDA == null)
                    mEmailContent_ResentNewDA = WebConfigurationManager.AppSettings["EmailContent_ResentNewDA"];

                return mEmailContent_ResentNewDA;
            }
        }
        #endregion

        #region Reporting Services
        private static string mReportFolder = null;
        public static string ReportFolder
        {
            get
            {
                if (mReportFolder == null)
                    mReportFolder = WebConfigurationManager.AppSettings["ReportFolder"];

                return mReportFolder;
            }
        }
        #endregion

        #region Pagination
        public static int PageSize
        {
            get
            {
                return Convert.ToInt32(WebConfigurationManager.AppSettings["PageSize"]);
            }
        }

        public static int PageRange
        {
            get
            {
                return Convert.ToInt32(WebConfigurationManager.AppSettings["PageRange"]);
            }
        }
        #endregion

        //Thien NGUYEN - DA Evolution - Generate PDF File for CR Block - 2018/03/11
        private static string mCRBlockPdfName = null;
        public static string CRBlockPdfName
        {
            get
            {
                if (mCRBlockPdfName == null)
                    mCRBlockPdfName = WebConfigurationManager.AppSettings["CRBlockPdfName"];

                return mCRBlockPdfName;
            }
        }

        #region For Attach File
        //Nga NGUYEN - DA Evolution - Attach Files
        private static int mFileSize = 0;
        public static int FileSize
        {
            get
            {
                if (mFileSize == 0)
                    mFileSize = int.Parse(WebConfigurationManager.AppSettings["FileSize"]);

                return mFileSize;
            }
        }
        private static string mFileExtension = null;
        public static string FileExtension
        {
            get
            {
                if (mFileExtension == null)
                    mFileExtension = WebConfigurationManager.AppSettings["FileExtension"];

                return mFileExtension;
            }
        }
        private static string mFolderPath = null;
        public static string FolderPath
        {
            get
            {
                if (mFolderPath == null)
                    mFolderPath = WebConfigurationManager.AppSettings["FolderPath"];

                return mFolderPath;
            }
        }
        private static string mIconFolerPath = null;
        public static string IconFolerPath
        {
            get
            {
                if (mIconFolerPath == null)
                    mIconFolerPath = WebConfigurationManager.AppSettings["IconFolerPath"];

                return mIconFolerPath;
            }
        }
        #endregion

        #region For Check Max Length CR Comment
        //private static string mMaxLengthForResponseComment = null;
        //public static string MaxLengthForResponseComment
        //{
        //    get
        //    {
        //        if (mMaxLengthForResponseComment == null)
        //            mMaxLengthForResponseComment = WebConfigurationManager.AppSettings["MaxLengthForResponseComment"];

        //        return mMaxLengthForResponseComment;
        //    }
        //}

        //private static string mMaxLengthForRelayComment = null;
        //public static string MaxLengthForRelayComment
        //{
        //    get
        //    {
        //        if (mMaxLengthForRelayComment == null)
        //            mMaxLengthForRelayComment = WebConfigurationManager.AppSettings["MaxLengthForRelayComment"];

        //        return mMaxLengthForRelayComment;
        //    }
        //}

        //private static string mMaxLengthForCloseComment = null;
        //public static string MaxLengthForCloseComment
        //{
        //    get
        //    {
        //        if (mMaxLengthForCloseComment == null)
        //            mMaxLengthForCloseComment = WebConfigurationManager.AppSettings["MaxLengthForCloseComment"];

        //        return mMaxLengthForCloseComment;
        //    }
        //}
        private static string mMaxLengthForCRComment = null;
        public static string MaxLengthForCRComment
        {
            get
            {
                if (mMaxLengthForCRComment == null)
                    mMaxLengthForCRComment = WebConfigurationManager.AppSettings["MaxLengthForCRComment"];

                return mMaxLengthForCRComment;
            }
        }
        #endregion

        #region Anonymous User
        private static string mAnonymousPersonFirstName = null;
        public static string AnonymousPersonFirstName
        {
            get
            {
                if (mAnonymousPersonFirstName == null)
                    mAnonymousPersonFirstName = WebConfigurationManager.AppSettings["AnonymousPersonFirstName"];

                return mAnonymousPersonFirstName;
            }
        }

        private static string mAnonymousPersonLastName = null;
        public static string AnonymousPersonLastName
        {
            get
            {
                if (mAnonymousPersonLastName == null)
                    mAnonymousPersonLastName = WebConfigurationManager.AppSettings["AnonymousPersonLastName"];

                return mAnonymousPersonLastName;
            }
        }
        #endregion
    }
}
