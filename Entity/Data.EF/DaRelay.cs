﻿using System;
using System.Collections.Generic;

namespace Entity.Data.EF
{
    public partial class DaRelay
    {
        public DaRelay()
        {
            KeywordRelay = new HashSet<KeywordRelay>();
        }

        public int FileNo { get; set; }
        public string CrRelayComment { get; set; }
        public DateTime? RelayedDate { get; set; }

        public Da FileNoNavigation { get; set; }
        public ICollection<KeywordRelay> KeywordRelay { get; set; }
    }
}
