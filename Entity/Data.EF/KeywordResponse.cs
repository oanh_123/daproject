﻿using System;
using System.Collections.Generic;

namespace Entity.Data.EF
{
    public partial class KeywordResponse
    {
        public int FileNo { get; set; }
        public int KeywordId { get; set; }

        public DaResponse FileNoNavigation { get; set; }
        public KeyWord Keyword { get; set; }
    }
}
