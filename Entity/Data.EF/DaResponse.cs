﻿using System;
using System.Collections.Generic;

namespace Entity.Data.EF
{
    public partial class DaResponse
    {
        public DaResponse()
        {
            KeywordResponse = new HashSet<KeywordResponse>();
        }

        public int FileNo { get; set; }
        public string CrResponseComment { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }

        public Da FileNoNavigation { get; set; }
        public ICollection<KeywordResponse> KeywordResponse { get; set; }
    }
}
