﻿using System;
using System.Collections.Generic;

namespace Entity.Data.EF
{
    public partial class UserProfile
    {
        public UserProfile()
        {
            UserRegion = new HashSet<UserRegion>();
        }

        public string UserId { get; set; }
        public int ProfileId { get; set; }
        public bool IsActive { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }

        public Profile Profile { get; set; }
        public ICollection<UserRegion> UserRegion { get; set; }
    }
}
