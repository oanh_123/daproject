﻿using System;
using System.Collections.Generic;

namespace Entity.Data.EF
{
    public partial class AttachedFiles
    {
        public int AttachedFileId { get; set; }
        public string FileName { get; set; }
        public string Comment { get; set; }
        public int FileNo { get; set; }

        public Da FileNoNavigation { get; set; }
    }
}
