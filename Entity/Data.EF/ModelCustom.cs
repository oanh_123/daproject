﻿using System.Collections.Generic;
namespace Entity.Data.EF
{
    public partial class UserProfile
    {
        public List<int> Regions { get; set; }

        public string AssinationDisplay
        {
            get
            {
                return IsActive == true ? "<span class=\"glyphicon glyphicon-ok\"  aria-hidden=\"true\"></span>" : "";
            }
        }

        public int ProfileIdOfCurrentUser { get; set; }

        public bool isFuncAdmin
        {
            get
            {
                if (ProfileIdOfCurrentUser == 3)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
    }

    public partial class Site
    {
        public string AssinationDisplay
        {
            get
            {
                return IsActive == true ? "<span class=\"glyphicon glyphicon-ok\"  aria-hidden=\"true\"></span>" : "";
            }
        }

        public string RegionName
        {
            get
            {
                return Region.Entity.EntityName + " - " + Region.RegionName;
            }
        }

    }

    public partial class CallObject
    {
        public string AssinationDisplay
        {
            get
            {
                return IsActive == true ? "<span class=\"glyphicon glyphicon-ok\"  aria-hidden=\"true\"></span>" : "";
            }
        }
    }

    public partial class KeyWord
    {
        public string AssinationDisplay
        {
            get
            {
                return IsActive == true ? "<span class=\"glyphicon glyphicon-ok\"  aria-hidden=\"true\"></span>" : "";
            }
        }
    }

    public partial class Region
    {
        public string RegionEntityName
        {
            get
            {
                return Entity.EntityName + " - " + RegionName;
            }
        }
        public string AssinationDisplay
        {
            get
            {
                return IsActive == true ? "<span class=\"glyphicon glyphicon-ok\"  aria-hidden=\"true\"></span>" : "";
            }
        }
    }

    public partial class DaRelay
        {
            public string UpdatedBy { get; set; }
        }
    
}
