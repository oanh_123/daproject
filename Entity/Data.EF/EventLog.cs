﻿using System;
using System.Collections.Generic;

namespace Entity.Data.EF
{
    public partial class EventLog
    {
        public int DossierNo { get; set; }
        public string Action { get; set; }
        public string Description { get; set; }
        public string EmailSentTo { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CreatedBy { get; set; }
    }
}
