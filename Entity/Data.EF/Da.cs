﻿using System;
using System.Collections.Generic;

namespace Entity.Data.EF
{
    public partial class Da
    {
        public Da()
        {
            AttachedFiles = new HashSet<AttachedFiles>();
        }

        public int FileNo { get; set; }
        public DateTime? CallingDate { get; set; }
        public string CallingTime { get; set; }
        public int? CallObjId { get; set; }
        public string SuffererGgid { get; set; }
        public string SuffererFirstName { get; set; }
        public string SuffererLastName { get; set; }
        public string SuffererPhoneNo { get; set; }
        public string CallerGgid { get; set; }
        public string CallerFirstName { get; set; }
        public string CallerLastName { get; set; }
        public string CallerPhoneNo { get; set; }
        public string City { get; set; }
        public int? SiteId { get; set; }
        public bool? IsInvolvedOnProject { get; set; }
        public string Pcm { get; set; }
        public int? Status { get; set; }
        public string SuffererMobile { get; set; }
        public string CallerMobile { get; set; }
        public bool? IsSuffererAgreed { get; set; }
        public int? SuffererAgeRange { get; set; }
        public DateTime? ContactDate { get; set; }
        public int? ContactMode { get; set; }

        public CallObject CallObj { get; set; }
        public Site Site { get; set; }
        public Status StatusNavigation { get; set; }
        public AgeRange SuffererAgeRangeNavigation { get; set; }
        public DaClose DaClose { get; set; }
        public DaRelay DaRelay { get; set; }
        public DaResponse DaResponse { get; set; }
        public ICollection<AttachedFiles> AttachedFiles { get; set; }
    }
}
