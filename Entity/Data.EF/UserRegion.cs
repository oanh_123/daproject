﻿using System;
using System.Collections.Generic;

namespace Entity.Data.EF
{
    public partial class UserRegion
    {
        public string UserId { get; set; }
        public int RegionId { get; set; }
        public bool IsActive { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }

        public Region Region { get; set; }
        public UserProfile User { get; set; }
    }
}
