﻿using System;
using System.Collections.Generic;

namespace Entity.Data.EF
{
    public partial class Region
    {
        public Region()
        {
            Site = new HashSet<Site>();
            UserRegion = new HashSet<UserRegion>();
        }

        public int RegionId { get; set; }
        public string RegionName { get; set; }
        public bool IsActive { get; set; }
        public int EntityId { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }

        public Entity Entity { get; set; }
        public ICollection<Site> Site { get; set; }
        public ICollection<UserRegion> UserRegion { get; set; }
    }
}
