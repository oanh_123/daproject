﻿using System;
using System.Collections.Generic;

namespace Entity.Data.EF
{
    public partial class UsersLog
    {
        public int Id { get; set; }
        public string LoginId { get; set; }
        public DateTime EventDate { get; set; }
        public string Action { get; set; }
        public string Detail { get; set; }
    }
}
