﻿using System;
using System.Collections.Generic;

namespace Entity.Data.EF
{
    public partial class DaClose
    {
        public int FileNo { get; set; }
        public string CrCloseComment { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }

        public Da FileNoNavigation { get; set; }
    }
}
