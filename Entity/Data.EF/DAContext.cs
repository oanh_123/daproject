﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Entity.Data.EF
{
    public partial class DAContext : DbContext
    {
        public DAContext() 
        {           
        }

        public DAContext(DbContextOptions<DAContext> options)
            : base(options)
        {
        }

        public virtual DbSet<AgeRange> AgeRange { get; set; }
        public virtual DbSet<AttachedFiles> AttachedFiles { get; set; }
        public virtual DbSet<CallObject> CallObject { get; set; }
        public virtual DbSet<Da> Da { get; set; }
        public virtual DbSet<DaClose> DaClose { get; set; }
        public virtual DbSet<DaRelay> DaRelay { get; set; }
        public virtual DbSet<DaResponse> DaResponse { get; set; }
        public virtual DbSet<Entity> Entity { get; set; }
        public virtual DbSet<EventLog> EventLog { get; set; }
        public virtual DbSet<FileNoManagement> FileNoManagement { get; set; }
        public virtual DbSet<KeyWord> KeyWord { get; set; }
        public virtual DbSet<KeywordRelay> KeywordRelay { get; set; }
        public virtual DbSet<KeywordResponse> KeywordResponse { get; set; }
        public virtual DbSet<Profile> Profile { get; set; }
        public virtual DbSet<Region> Region { get; set; }
        public virtual DbSet<RelayLog> RelayLog { get; set; }
        public virtual DbSet<RoleMatrix> RoleMatrix { get; set; }
        public virtual DbSet<Site> Site { get; set; }
        public virtual DbSet<Status> Status { get; set; }
        public virtual DbSet<UserProfile> UserProfile { get; set; }
        public virtual DbSet<UserRegion> UserRegion { get; set; }
        public virtual DbSet<UsersLog> UsersLog { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
//#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
//                optionsBuilder.UseSqlServer("Data Source=LVN01000031;Initial Catalog=DA;Integrated Security=True");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AgeRange>(entity =>
            {
                entity.Property(e => e.AgeRangeId).ValueGeneratedNever();

                entity.Property(e => e.Detail).HasMaxLength(100);
            });

            modelBuilder.Entity<AttachedFiles>(entity =>
            {
                entity.HasKey(e => e.AttachedFileId);

                entity.Property(e => e.AttachedFileId).HasColumnName("AttachedFileID");

                entity.Property(e => e.Comment).HasMaxLength(500);

                entity.Property(e => e.FileName)
                    .IsRequired()
                    .HasMaxLength(200);

                entity.HasOne(d => d.FileNoNavigation)
                    .WithMany(p => p.AttachedFiles)
                    .HasForeignKey(d => d.FileNo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_AttachedFiles_DA");
            });

            modelBuilder.Entity<CallObject>(entity =>
            {
                entity.HasKey(e => e.CallObjId);

                entity.Property(e => e.CallObjId).HasColumnName("CallObjID");

                entity.Property(e => e.CallObjName).HasMaxLength(100);

                entity.Property(e => e.UpdatedBy)
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.UpdatedDate).HasColumnType("date");
            });

            modelBuilder.Entity<Da>(entity =>
            {
                entity.HasKey(e => e.FileNo);

                entity.ToTable("DA");

                entity.Property(e => e.FileNo).ValueGeneratedNever();

                entity.Property(e => e.CallObjId).HasColumnName("CallObjID");

                entity.Property(e => e.CallerFirstName).HasMaxLength(100);

                entity.Property(e => e.CallerGgid)
                    .HasColumnName("CallerGGID")
                    .HasMaxLength(60)
                    .IsUnicode(false);

                entity.Property(e => e.CallerLastName).HasMaxLength(100);

                entity.Property(e => e.CallerMobile)
                    .HasMaxLength(60)
                    .IsUnicode(false);

                entity.Property(e => e.CallerPhoneNo)
                    .HasMaxLength(60)
                    .IsUnicode(false);

                entity.Property(e => e.CallingDate).HasColumnType("date");

                entity.Property(e => e.CallingTime).HasMaxLength(5);

                entity.Property(e => e.City).HasMaxLength(100);

                entity.Property(e => e.ContactDate).HasColumnType("date");

                entity.Property(e => e.Pcm)
                    .HasColumnName("PCM")
                    .HasMaxLength(500);

                entity.Property(e => e.SiteId).HasColumnName("SiteID");

                entity.Property(e => e.SuffererFirstName).HasMaxLength(100);

                entity.Property(e => e.SuffererGgid)
                    .HasColumnName("SuffererGGID")
                    .HasMaxLength(60)
                    .IsUnicode(false);

                entity.Property(e => e.SuffererLastName).HasMaxLength(100);

                entity.Property(e => e.SuffererMobile)
                    .HasMaxLength(60)
                    .IsUnicode(false);

                entity.Property(e => e.SuffererPhoneNo)
                    .HasMaxLength(60)
                    .IsUnicode(false);

                entity.HasOne(d => d.CallObj)
                    .WithMany(p => p.Da)
                    .HasForeignKey(d => d.CallObjId)
                    .HasConstraintName("FK__DA__CallObjID__59063A47");

                entity.HasOne(d => d.Site)
                    .WithMany(p => p.Da)
                    .HasForeignKey(d => d.SiteId)
                    .HasConstraintName("FK__DA__SiteID__59FA5E80");

                entity.HasOne(d => d.StatusNavigation)
                    .WithMany(p => p.Da)
                    .HasForeignKey(d => d.Status)
                    .HasConstraintName("FK_DA_Status");

                entity.HasOne(d => d.SuffererAgeRangeNavigation)
                    .WithMany(p => p.Da)
                    .HasForeignKey(d => d.SuffererAgeRange)     
                    .HasConstraintName("FK_DA_AgeRange");
            });

            modelBuilder.Entity<DaClose>(entity =>
            {
                entity.HasKey(e => e.FileNo);

                entity.ToTable("DA_CLOSE");

                entity.Property(e => e.FileNo).ValueGeneratedNever();

                entity.Property(e => e.CrCloseComment)
                    .HasColumnName("CR_CloseComment")
                    .IsUnicode(false);

                entity.Property(e => e.UpdatedBy)
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.UpdatedDate).HasColumnType("date");

                entity.HasOne(d => d.FileNoNavigation)
                    .WithOne(p => p.DaClose)
                    .HasForeignKey<DaClose>(d => d.FileNo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__DA_CLOSE__FileNo__6A30C649");
            });

            modelBuilder.Entity<DaRelay>(entity =>
            {
                entity.HasKey(e => e.FileNo);

                entity.ToTable("DA_RELAY");

                entity.Property(e => e.FileNo).ValueGeneratedNever();

                entity.Property(e => e.CrRelayComment)
                    .HasColumnName("CR_RelayComment")
                    .IsUnicode(false);

                entity.Property(e => e.RelayedDate).HasColumnType("date");

                entity.HasOne(d => d.FileNoNavigation)
                    .WithOne(p => p.DaRelay)
                    .HasForeignKey<DaRelay>(d => d.FileNo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__DA_RELAY__FileNo__4B7734FF");
            });

            modelBuilder.Entity<DaResponse>(entity =>
            {
                entity.HasKey(e => e.FileNo);

                entity.ToTable("DA_RESPONSE");

                entity.Property(e => e.FileNo).ValueGeneratedNever();

                entity.Property(e => e.CrResponseComment)
                    .HasColumnName("CR_ResponseComment")
                    .IsUnicode(false);

                entity.Property(e => e.UpdatedBy)
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.UpdatedDate).HasColumnType("date");

                entity.HasOne(d => d.FileNoNavigation)
                    .WithOne(p => p.DaResponse)
                    .HasForeignKey<DaResponse>(d => d.FileNo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__DA_RESPON__FileN__6FE99F9F");
            });

            modelBuilder.Entity<Entity>(entity =>
            {
                entity.Property(e => e.EntityId)
                    .HasColumnName("EntityID")
                    .ValueGeneratedNever();

                entity.Property(e => e.EntityName)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.UpdatedBy)
                    .IsRequired()
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.UpdatedDate).HasColumnType("date");
            });

            modelBuilder.Entity<EventLog>(entity =>
            {
                entity.HasKey(e => new { e.DossierNo, e.Action, e.CreatedDate });

                entity.Property(e => e.Action)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedDate).HasColumnType("datetime");

                entity.Property(e => e.CreatedBy)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Description).HasMaxLength(200);

                entity.Property(e => e.EmailSentTo)
                    .HasMaxLength(300)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<FileNoManagement>(entity =>
            {
                entity.HasKey(e => e.CurrentFileNo);

                entity.Property(e => e.CurrentFileNo).ValueGeneratedNever();
            });

            modelBuilder.Entity<KeyWord>(entity =>
            {
                entity.Property(e => e.KeyWordId).HasColumnName("KeyWordID");

                entity.Property(e => e.KeyWordName).HasMaxLength(100);

                entity.Property(e => e.UpdatedBy)
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.UpdatedDate).HasColumnType("date");
            });

            modelBuilder.Entity<KeywordRelay>(entity =>
            {
                entity.HasKey(e => new { e.FileNo, e.KeywordId });

                entity.HasOne(d => d.FileNoNavigation)
                    .WithMany(p => p.KeywordRelay)
                    .HasForeignKey(d => d.FileNo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_KeywordRelay_DA_RELAY");

                entity.HasOne(d => d.Keyword)
                    .WithMany(p => p.KeywordRelay)
                    .HasForeignKey(d => d.KeywordId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_KeywordRelay_KeyWord");
            });

            modelBuilder.Entity<KeywordResponse>(entity =>
            {
                entity.HasKey(e => new { e.FileNo, e.KeywordId });

                entity.HasOne(d => d.FileNoNavigation)
                    .WithMany(p => p.KeywordResponse)
                    .HasForeignKey(d => d.FileNo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_KeywordResponse_DA_RESPONSE");

                entity.HasOne(d => d.Keyword)
                    .WithMany(p => p.KeywordResponse)
                    .HasForeignKey(d => d.KeywordId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_KeywordResponse_KeyWord");
            });

            modelBuilder.Entity<Profile>(entity =>
            {
                entity.Property(e => e.ProfileId)
                    .HasColumnName("ProfileID")
                    .ValueGeneratedNever();

                entity.Property(e => e.ProfileName)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.ProfileSignature).HasMaxLength(100);
            });

            modelBuilder.Entity<Region>(entity =>
            {
                entity.Property(e => e.RegionId).HasColumnName("RegionID");

                entity.Property(e => e.EntityId).HasColumnName("EntityID");

                entity.Property(e => e.RegionName)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.UpdatedBy)
                    .IsRequired()
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.UpdatedDate).HasColumnType("date");

                entity.HasOne(d => d.Entity)
                    .WithMany(p => p.Region)
                    .HasForeignKey(d => d.EntityId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Region_Entity");
            });

            modelBuilder.Entity<RelayLog>(entity =>
            {
                entity.HasKey(e => new { e.FileNo, e.UpdatedDate });

                entity.Property(e => e.UpdatedDate).HasColumnType("datetime");

                entity.Property(e => e.CrRelayComment).HasColumnName("CR_RelayComment");

                entity.Property(e => e.UpdatedBy)
                    .IsRequired()
                    .HasMaxLength(8)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<RoleMatrix>(entity =>
            {
                entity.HasKey(e => e.ScreenId);

                entity.Property(e => e.ScreenId).HasColumnName("ScreenID");

                entity.Property(e => e.ScreenName)
                    .IsRequired()
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<Site>(entity =>
            {
                entity.Property(e => e.SiteId).HasColumnName("SiteID");

                entity.Property(e => e.RegionId).HasColumnName("RegionID");

                entity.Property(e => e.SiteName)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.UpdatedBy)
                    .IsRequired()
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.UpdatedDate).HasColumnType("date");

                entity.HasOne(d => d.Region)
                    .WithMany(p => p.Site)
                    .HasForeignKey(d => d.RegionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Site_Region");
            });

            modelBuilder.Entity<Status>(entity =>
            {
                entity.HasKey(e => e.Status1);

                entity.Property(e => e.Status1)
                    .HasColumnName("Status")
                    .ValueGeneratedNever();

                entity.Property(e => e.Description).HasMaxLength(20);

                entity.Property(e => e.DescriptionFrench).HasMaxLength(20);
            });

            modelBuilder.Entity<UserProfile>(entity =>
            {
                entity.HasKey(e => e.UserId);

                entity.Property(e => e.UserId)
                    .HasColumnName("UserID")
                    .HasMaxLength(8)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.Email)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.FirstName).HasMaxLength(100);

                entity.Property(e => e.LastName).HasMaxLength(100);

                entity.Property(e => e.ProfileId).HasColumnName("ProfileID");

                entity.Property(e => e.UpdatedBy)
                    .IsRequired()
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.UpdatedDate).HasColumnType("date");

                entity.HasOne(d => d.Profile)
                    .WithMany(p => p.UserProfile)
                    .HasForeignKey(d => d.ProfileId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__UserProfi__Profi__04AFB25B");
            });

            modelBuilder.Entity<UserRegion>(entity =>
            {
                entity.HasKey(e => new { e.UserId, e.RegionId });

                entity.Property(e => e.UserId)
                    .HasColumnName("UserID")
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.RegionId).HasColumnName("RegionID");

                entity.Property(e => e.UpdatedBy)
                    .IsRequired()
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.UpdatedDate).HasColumnType("date");

                entity.HasOne(d => d.Region)
                    .WithMany(p => p.UserRegion)
                    .HasForeignKey(d => d.RegionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__UserRegio__Regio__634EBE90");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.UserRegion)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_UserRegion_UserProfile");
            });

            modelBuilder.Entity<UsersLog>(entity =>
            {
                entity.Property(e => e.Action)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Detail)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.EventDate).HasColumnType("datetime");

                entity.Property(e => e.LoginId)
                    .IsRequired()
                    .HasMaxLength(8)
                    .IsUnicode(false);
            });
        }
    }
}
