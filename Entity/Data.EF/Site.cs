﻿using System;
using System.Collections.Generic;

namespace Entity.Data.EF
{
    public partial class Site
    {
        public Site()
        {
            Da = new HashSet<Da>();
        }

        public int SiteId { get; set; }
        public string SiteName { get; set; }
        public bool IsActive { get; set; }
        public int RegionId { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }

        public Region Region { get; set; }
        public ICollection<Da> Da { get; set; }
    }
}
