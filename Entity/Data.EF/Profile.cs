﻿using System;
using System.Collections.Generic;

namespace Entity.Data.EF
{
    public partial class Profile
    {
        public Profile()
        {
            UserProfile = new HashSet<UserProfile>();
        }

        public int ProfileId { get; set; }
        public string ProfileName { get; set; }
        public string ProfileSignature { get; set; }

        public ICollection<UserProfile> UserProfile { get; set; }
    }
}
