﻿using System;
using System.Collections.Generic;

namespace Entity.Data.EF
{
    public partial class Status
    {
        public Status()
        {
            Da = new HashSet<Da>();
        }

        public int Status1 { get; set; }
        public string Description { get; set; }
        public string DescriptionFrench { get; set; }

        public ICollection<Da> Da { get; set; }
    }
}
