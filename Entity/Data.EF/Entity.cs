﻿using System;
using System.Collections.Generic;

namespace Entity.Data.EF
{
    public partial class Entity
    {
        public Entity()
        {
            Region = new HashSet<Region>();
        }

        public int EntityId { get; set; }
        public string EntityName { get; set; }
        public bool IsActive { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }

        public ICollection<Region> Region { get; set; }
    }
}
