﻿using System;
using System.Collections.Generic;

namespace Entity.Data.EF
{
    public partial class RoleMatrix
    {
        public int ScreenId { get; set; }
        public string ScreenName { get; set; }
        public bool RelayAlert { get; set; }
        public bool External { get; set; }
        public bool FunctionalAdmin { get; set; }
        public bool TechnicalAdmin { get; set; }
    }
}
