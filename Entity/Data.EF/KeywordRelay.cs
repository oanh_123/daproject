﻿using System;
using System.Collections.Generic;

namespace Entity.Data.EF
{
    public partial class KeywordRelay
    {
        public int FileNo { get; set; }
        public int KeywordId { get; set; }

        public DaRelay FileNoNavigation { get; set; }
        public KeyWord Keyword { get; set; }
    }
}
