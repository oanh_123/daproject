﻿using System;
using System.Collections.Generic;

namespace Entity.Data.EF
{
    public partial class KeyWord
    {
        public KeyWord()
        {
            KeywordRelay = new HashSet<KeywordRelay>();
            KeywordResponse = new HashSet<KeywordResponse>();
        }

        public int KeyWordId { get; set; }
        public string KeyWordName { get; set; }
        public bool? IsActive { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }

        public ICollection<KeywordRelay> KeywordRelay { get; set; }
        public ICollection<KeywordResponse> KeywordResponse { get; set; }
    }
}
