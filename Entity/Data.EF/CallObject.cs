﻿using System;
using System.Collections.Generic;

namespace Entity.Data.EF
{
    public partial class CallObject
    {
        public CallObject()
        {
            Da = new HashSet<Da>();
        }

        public int CallObjId { get; set; }
        public string CallObjName { get; set; }
        public bool? IsActive { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }

        public ICollection<Da> Da { get; set; }
    }
}
