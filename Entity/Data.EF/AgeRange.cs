﻿using System;
using System.Collections.Generic;

namespace Entity.Data.EF
{
    public partial class AgeRange
    {
        public AgeRange()
        {
            Da = new HashSet<Da>();
        }

        public int AgeRangeId { get; set; }
        public string Detail { get; set; }

        public ICollection<Da> Da { get; set; }
    }
}
