﻿using System;
using System.Collections.Generic;

namespace Entity.Data.EF
{
    public partial class RelayLog
    {
        public int FileNo { get; set; }
        public string CrRelayComment { get; set; }
        public DateTime UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }
    }
}
