﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entity.Data.EF
{
    public class DACustom
    {
        public int FileNo { get; set; }
        public string FileNoString { get; set; }
        public Nullable<System.DateTime> CallingDate { get; set; }
        public Nullable<int> CallObjID { get; set; }
        public string CallObjName { get; set; }
        public Nullable<int> SiteID { get; set; }
        public string SiteName { get; set; }
        public Nullable<int> EntityID { get; set; }
        public string EntityName { get; set; }
        public Nullable<int> RegionID { get; set; }
        public Nullable<int> Status { get; set; }
        public string StatusDescription { get; set; }
        public string StatusFrenchDescription { get; set; }
    }
}
