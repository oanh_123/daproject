﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DA.Repositories.Repositories
{
   public interface IRepository<TEntity> where TEntity : class
    {
        TEntity Find(params object[] keyValues);
        Task<IEnumerable<TEntity>> GetAllAsync();
        Task<TEntity> GetByIdAsync(params object[] keyValues);
        Task<IEnumerable<TEntity>> GetAsync(Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            string includeProperties = "",
            int? page = null,
            int? pageSize = null);

        void Insert(TEntity entity);
        TEntity InsertAndGet(TEntity entity);
        void Update(TEntity entity);
        void Delete(TEntity entity);
        void Delete(object id);
        void Delete(object id1, object id2);
        void InsertRange(IEnumerable<TEntity> entities);
    }
   

    
}
