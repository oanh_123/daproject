﻿using Entity.Data.EF;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DA.Repositories.Repositories
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        internal DAContext Context;
        internal DbSet<TEntity> DbSet;
        public Repository(DAContext contextEntities)
        {
            this.Context = contextEntities;
            this.DbSet = this.Context.Set<TEntity>();
        }
        public Repository()
        {
            this.Context = new DAContext();
            this.DbSet = this.Context.Set<TEntity>();

        }
        public void Delete(TEntity entity)
        {
            if(Context.Entry(entity).State == EntityState.Detached)
            {
                DbSet.Attach(entity);
            }
            DbSet.Remove(entity);
        }
        public async Task<IEnumerable<TEntity>> GetAllAsync()
        {
            return await DbSet.ToListAsync();
        }

        public void Delete(object id)
        {
            var entity = DbSet.Find(id);
            Delete(entity);
        }

        public void Delete(object id1, object id2)
        {
            var entity = DbSet.Find(id1, id2);
            Delete(entity);
        }

        public TEntity Find(params object[] keyValues)
        {
            return DbSet.Find(keyValues);
        }

        public async Task<IEnumerable<TEntity>> GetAsync(Expression<Func<TEntity, bool>> filter = null, Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null, string includeProperties = "", int? page = null, int? pageSize = null)
        {
            IQueryable<TEntity> query = DbSet;
            foreach (var includeProperty in includeProperties.Split
                (new char[] { ','}, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }
            if(filter != null)
            {
                query = query.Where(filter);
            }
            if(orderBy != null)
            {
                query = orderBy(query);
            }
            if(page != null && pageSize != null)
            {
                query = query
                    .Skip((page.Value - 1) * pageSize.Value)
                    .Take(pageSize.Value);
            }
            return query.ToList();
        }

        public void Insert(TEntity entity)
        {
            DbSet.Add(entity);
        }

        public TEntity InsertAndGet(TEntity entity)
        {
            return DbSet.Add(entity).Entity;   
        }

        public void Update(TEntity entity)
        {
            DbSet.Attach(entity);
            Context.Entry(entity).State = EntityState.Modified;

        }
        public void InsertRange(IEnumerable<TEntity> entities)
        {
            DbSet.AddRange(entities);
        }

        public async Task<TEntity> GetByIdAsync(params object[] keyValues)
        {
            return await DbSet.FindAsync(keyValues);
        }
    }
}
