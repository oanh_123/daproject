﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DA.Repositories.Repositories
{
    public interface IUnitOfWork : IDisposable
    {
        Task<int> Save();
        Task Commit();
        void RollBack();
    }
}
