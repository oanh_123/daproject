﻿using Entity.Data.EF;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DA.Repositories.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private DAContext _context;
        private bool _disposed;
        public UnitOfWork(DAContext context)
        {
            _context = context;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public virtual void Dispose(bool disposing)
        {
            if (!_disposed)
               if (disposing)
                    _context.Dispose();
            _disposed = true;
        }

        public void RollBack()
        {
            _context
                .ChangeTracker
                .Entries()
                .ToList()
                .ForEach(x => x.Reload());
        }


       public async Task<int> Save()
        {
            return await _context.SaveChangesAsync();
        }

        public async Task Commit()
        {
            await _context.SaveChangesAsync();
        }
    }
}
