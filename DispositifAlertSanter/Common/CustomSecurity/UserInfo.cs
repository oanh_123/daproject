﻿using DA.Business.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DispositifAlertSanter.Common
{
    public class UserInfo
    {
        public string UserID { get; set; }
        public bool IsActive { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public System.DateTime UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public int ProfileID { get; set; }
        public string ProfileName { get; set; }
        public string ProfileSignature { get; set; }
        public List<int> RegionsIDList { get; set; }
        public List<bool> RoleMatrixList { get; set; }

        public UserInfo()
        {
            this.UserID = string.Empty;
            this.IsActive = false;
            this.FirstName = string.Empty;
            this.LastName = string.Empty;
            this.Email = string.Empty;
            this.UpdatedDate = DateTime.Now;
            this.UpdatedBy = string.Empty;
            this.ProfileID = 0;
            this.RegionsIDList = new List<int>();
            this.RoleMatrixList = new List<bool>();
        }
       
    }
}
